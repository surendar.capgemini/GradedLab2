package com.retail.web;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/Destination"},
		features = "src/resources/price.feature",  monochrome = true )
public class RunTests {

}
