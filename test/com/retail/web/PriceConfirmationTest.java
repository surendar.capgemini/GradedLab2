package com.retail.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.retail.core.Employees;
import com.retail.core.JenkinsScreenshot;
import com.retail.core.Resource;

public class PriceConfirmationTest {

	private ChromeDriver driver;
	private List<Resource> resources;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\surmeesa\\Desktop\\Git_Labs\\SimpleRetailWeb\\chromedriver.exe");
		driver = new ChromeDriver();
		resources = new ArrayList<Resource>();
	}

	@Test
	public void testgetResources() {
		resources.add(new Resource("456", "Justin", "Poole", 44.0));
		resources.add(new Resource("567", "Mark", "Zukerberg", 22.0));
		resources.add(new Resource("346", "John", "Smith", 33.0));
		resources.add(new Resource("276", "Chris", "Colin", 55.0));

		for (int i = 0; i < resources.size(); i++) {
			assertEquals(resources.get(i).getResourceId(), Employees.getResources().get(i).getResourceId());
			assertEquals(resources.get(i).getFirstName(), Employees.getResources().get(i).getFirstName());
			assertEquals(resources.get(i).getLastName(), Employees.getResources().get(i).getLastName());
			assertEquals(resources.get(i).getNoofHours(), Employees.getResources().get(i).getNoofHours(), .001);
		}
	}

	@Test
	public void test() {
		driver.get("http://localhost:8080/SimpleRetailWeb/");
		WebElement btn = driver.findElement(By.name("shipButton"));
		btn.submit();

		List<WebElement> weightElements = driver.findElements(By.xpath("//tbody/tr/td[4]"));

		String[] expectedWeights = { "19.99", "17.99", "20.49", "23.88", "9.75", "17.25" };
		for (WebElement webElement : weightElements) {
			System.out.println(webElement.getText());
			assertTrue(Arrays.asList(expectedWeights).contains(webElement.getText()));
		}
	}

	@Test
	public void testJenkinsScreenshot() {
		assertTrue(JenkinsScreenshot.isTakeScreenshot(driver, "JenkinsScreenshot"));
	}

	@After
	public void quit() {
		driver.close();
		driver.quit();
	}

}
