package com.retail.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PriceStepDefinition {
	private ChromeDriver driver;
	private Map<String, Double> upcPriceMap;
	private String empName;

	@Before("@Web")
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\surmeesa\\Desktop\\Git_Labs\\SimpleRetailWeb\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("^the following Data:$")
	public void the_following_Data(DataTable item) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		// throw new PendingException();

		upcPriceMap = item.asMap(String.class, Double.class);
		System.out.println("\n\n***upcPriceMap size:" + upcPriceMap.size());
	}

	@When("^User Navigates to Create Shipment Page$")
	public void user_Navigates_to_Create_Shipment_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();

		driver.get("http://localhost:8080/SimpleRetailWeb/"); 
		driver.manage().window().maximize();
		WebElement btn = driver.findElement(By.name("shipButton"));
		btn.submit();
		assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));
	}

	@Then("^UPC in page should match the given price$")
	public void upc_in_page_should_match_the_given_price() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();

		List<WebElement> upcElements = driver.findElements(By.xpath("//tbody/tr/td[2]"));
		List<WebElement> priceElements = driver.findElements(By.xpath("//tbody/tr/td[4]"));

		Map<String, Double> weightPriceMapFromUI = new HashMap<>();

		for (int i = 0; i < upcElements.size(); i++) {
			weightPriceMapFromUI.put(upcElements.get(i).getText(), Double.parseDouble(priceElements.get(i).getText()));
		}

		for (int i = 0; i < upcElements.size(); i++) {
			assertEquals(upcPriceMap.get(upcElements.get(i).getText()),
					Double.parseDouble(priceElements.get(i).getText()), 0.1);
		}
	}

	
	@Given("^Name of the employee is \"([^\"]*)\"$")
	public void name_of_the_employee_is(String name) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		empName = name;
	}

	@When("^I ask for a shipment$")
	public void i_ask_for_a_shipment() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.get("http://localhost:8080/SimpleRetailWeb/");
		driver.manage().window().maximize();
		WebElement btn = driver.findElement(By.name("shipButton"));
		btn.submit();
		driver.findElement(By.id("checkedRows")).click();
		driver.findElement(By.name("shipmentbtn")).click();
		
	}
	
	@Then("^His employee id will be \"([^\"]*)\"$")
	public void his_employee_id_will_be(String eid) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		WebElement s = driver.findElement(By.id("resourceId"));
		assertEquals(eid, s.getText());
	}

	@After("@Web")
	public void tearDown() {
		  driver.close(); 
		  driver.quit();
	}
}
