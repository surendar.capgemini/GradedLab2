Feature: UPC has the correct price
Ensure the each UPC has the correct price

@Web
Scenario: UPC and Price 
	Given the following Data:
	|567321101987	| 19.99 |
	|567321101986	| 17.99 |
	|567321101985	| 20.49 |
	|567321101984	| 23.88 |
	|467321101899	| 9.75  |
	|477321101878	| 17.25 |
	
	When User Navigates to Create Shipment Page
	
	Then UPC in page should match the given price

@Web
Scenario: Shipment EmpID Verification
	Given Name of the employee is "Mark"
	When I ask for a shipment
	Then His employee id will be "567"	
			