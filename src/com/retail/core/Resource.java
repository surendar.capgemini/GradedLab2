package com.retail.core;

import java.io.Serializable;

public class Resource implements Serializable{

	private static final long serialVersionUID = 1L;
	private String resourceId;
	private String firstName;
	private String lastName;
	private double noofHours;
	
	public Resource(String resourceId, String firstName, String lastName, double noofHours) {
		super();
		this.resourceId = resourceId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.noofHours = noofHours;
	}
	
	public String getResourceId() {
		return resourceId;
	}
	
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public double getNoofHours() {
		return noofHours;
	}
	
	public void setNoofHours(double noofHours) {
		if(noofHours > 40) {
			throw new IllegalArgumentException();
		}
		this.noofHours = noofHours;
	}
	
	@Override
	public String toString() {
		return "Resource [resourceId=" + resourceId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", noofHours=" + noofHours + "]";
	}
}
