package com.retail.core;

import java.util.ArrayList;
import java.util.List;

public class Employees {
	
	public static List<Resource> getResources() {

		List<Resource> resources = new ArrayList<Resource>();
		
		resources.add(new Resource("456", "Justin", "Poole", 44.0));
		resources.add(new Resource("567", "Mark", "Zukerberg", 22.0));
		resources.add(new Resource("346", "John", "Smith", 33.0));
		resources.add(new Resource("276", "Chris", "Colin", 55.0));
		
		return resources;
	}
	
}
