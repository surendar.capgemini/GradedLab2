package com.retail.core;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class JenkinsScreenshot {

	 public static boolean isTakeScreenshot(WebDriver driver, String imageName) {
		 
		boolean screenshotPlaced;
		driver.get("http://localhost:8081");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("j_username")).sendKeys("admin");
		driver.findElement(By.name("j_password")).sendKeys("9e8ef8a39cd54195b50d37f46d7e376b");
        driver.findElement(By.id("yui-gen1-button")).click();
		
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File("C:\\Users\\surmeesa\\Desktop\\Screenshot\\" + imageName+".jpg" ));
		} catch (IOException e) {
			e.printStackTrace();
		}
		screenshotPlaced = true;
		return screenshotPlaced; 
		
	}
}
